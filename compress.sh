#!/bin/bash
FILE=$1
DATE1=$(date +"%Y-%m-%d")
FILE_WITH_DATE="$FILE.$DATE1"
MINSIZE=5000000
ACTSIZE=$(wc -c <"$FILE")
if [ $ACTSIZE -ge $MINSIZE ]; then
	$(mv $FILE $FILE_WITH_DATE)
	$(gzip $FILE_WITH_DATE)
fi
