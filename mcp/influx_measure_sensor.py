from influxdb import InfluxDBClient
import datetime 

def get_influx_client():
    return InfluxDBClient('localhost', 8086, 'admin', 'vasudanin1', 'home_stats')

def get_humidity():
    f = open("/root/cron-scripts/mcp/mcp_output", "r")
    hum = f.read()
    save_dth11(hum)

def save_dth11(humidity, port="1"):
    json_body = [
        {
            "measurement": "sensor",
            "tags": {
                "GPIO_port": "GPIO_" + port,
            },
            "time": datetime.datetime.now().replace(microsecond=0).isoformat(),
            "fields": {
                "value": int(humidity),
            }
        }
    ]
    get_influx_client().write_points(json_body)


get_humidity()
